### A Pluto.jl notebook ###
# v0.19.0

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 39a0af94-6c34-4e41-b0d1-0bfa179b1c93
using Pkg

# ╔═╡ a08cb406-9453-4ce1-973f-228ad82937fb
Pkg.activate("Project.toml")

# ╔═╡ 300c3d14-0858-4c1e-aa4e-a7dc9d15666d
using PlutoUI

# ╔═╡ 6fbce7f8-ff9e-4666-8652-d52b5d0d5a24
using DataStructures

# ╔═╡ c5f9cfc6-0d5f-4479-bb9a-4ab888263334
using Markdown

# ╔═╡ 16aa00d7-dee3-49df-9e19-4b8a1caa566f
using InteractiveUtils

# ╔═╡ 7c74761a-cd32-4ee9-b76c-bbe5cfb9cc58
md"## Vetumbuavi Ndjavera
* Student no#: 217121314
"

# ╔═╡ b8ed9cf3-07fe-4c90-a87b-0d012fc57910


# ╔═╡ 870a474b-532f-4a37-b808-0a9122ff5a3f
md"## State def "

# ╔═╡ c1542490-5b5a-4874-9a34-140d694f2c9c
struct State
    name::String
    position::Int64
   parcels::Vector{Bool}
end

# ╔═╡ 325d896b-9435-4a3c-9c3b-0cca0a6d31f1
md"## Action Def"

# ╔═╡ 79c3c377-64db-4079-bcd1-4f3dcac7944d
struct Action
    name::String
    cost::Int64
end

# ╔═╡ 55fddaeb-3496-4f03-a70d-d83507afbd88
md"## Declaring the actions "

# ╔═╡ 5ae7f7a3-8848-44c8-ab8b-8e0dd22575e3
A1 = Action("me", 2)

# ╔═╡ d1ef21f4-d2c4-4e73-a318-7da539dc10da
A2 = Action("mw", 2)

# ╔═╡ 49240fc8-4694-4185-9f8f-a6168d46e38a
A3 = Action("mu", 1)

# ╔═╡ c829a6dc-3e59-478a-b9d3-8c0b24c28706
A4 = Action("md", 1)

# ╔═╡ d0c6d99f-9080-4b56-a8f0-93d26ae28d5e
A5 = Action("co", 5)

# ╔═╡ 24e97bc6-d419-4534-8a51-70bac191c281
md"## Declaring the states  "

# ╔═╡ 3bd8a2ca-ecf5-47e5-8187-07b862439725
S1 = State("Stateone", 1, [true, true, true])

# ╔═╡ 294ed3de-1688-437c-a67c-4031f5a85d02
S2 = State("Statetwo", 2, [true, true, true])

# ╔═╡ d8e5b6dd-ede8-4542-bae5-79bdda1b65a8
S3 = State("Statethree", 3, [true, true, true])

# ╔═╡ 1e474d08-a0bf-4f4f-a7d1-1650cda8cbf6
S4 = State("Statefour", 4, [true, true, true])

# ╔═╡ b7b78c22-9b53-41da-8f20-0ff4e22d27ad
S5 = State("Statefive", 5, [true, true, true])

# ╔═╡ fabc9a90-9717-4ab4-b774-77d93de01244
S6 = State("Statesix", 1, [false, true, true])

# ╔═╡ 7e9b68cd-79a6-4e14-b007-8e2f1640fc5e
S7 = State("Stateseven", 2, [false, true, true])

# ╔═╡ 5d5358aa-6d43-4fbc-9556-b22a0f1b0030
S8 = State("Stateeight", 3, [false, true, true])

# ╔═╡ 80c5988e-c141-49f2-a272-8689915d2473
S9 = State("Statenine", 4, [false, true, true])

# ╔═╡ 2bd6e9c1-dfd2-46fc-80ed-9b4228f0b888
S10 = State("Stateten", 5, [false, false, false])

# ╔═╡ d75e3c1a-da82-45a5-b4c6-1fcd07f32af3
md"###Transition model"

# ╔═╡ 2b1a9b5c-cafb-4a68-8ab7-eaf9dff462d8
Transition_Model = Dict()

# ╔═╡ 2f84da37-39c8-4cde-ae19-2c550b72caa5
md"### Add a mapping"

# ╔═╡ 1456cfad-c1d5-4bec-b337-e6705d40b186
push!(Transition_Model, S1 => [(A2, S2), (A5, S6), (A3, S1)])

# ╔═╡ da737b34-b067-46c9-93bc-cbde66244f9a
push!(Transition_Model, S1 => [(A2, S3), (A5, S7), (A1, S2)])

# ╔═╡ 7eec2e76-5d91-4def-926f-23eb3d031745
push!(Transition_Model, S1 => [(A4, S4), (A5, S8), (A1, S3)])

# ╔═╡ c3e99177-bd08-4dae-aa08-d455781f86a7
push!(Transition_Model, S1 => [(A2, S5), (A5, S9), (A3, S4)])

# ╔═╡ 8ca45d09-2f5e-4da5-8eeb-723adb379f9d
push!(Transition_Model, S10 => [(A2, S5), (A5, S9), (A4, S10)])

# ╔═╡ 4958cae9-9d0f-4ddb-849c-dc6132465a43
Transition_Model

# ╔═╡ 29a0dee6-a271-447e-97d6-cc617152f4d5
md"### Getting User Input"

# ╔═╡ 3ae1c473-5967-4bb1-b0d5-31f1b92507e9
@bind storeys TextField()

# ╔═╡ 5dffa43c-dc33-4cd7-b8cb-286e73e521ef
with_terminal() do
	println(storeys)
end

# ╔═╡ a02c4a9b-798b-4cf4-8d93-fccadfb8caef
@bind offices_per_floor TextField()

# ╔═╡ fb3b021e-9aee-477d-896b-3f53fa755b7f
with_terminal() do
	println(offices_per_floor)
end

# ╔═╡ 3a8c7fc0-ae7d-4c85-9bba-3a8e9c4afe60
@bind parcels_in_each_office TextField()

# ╔═╡ 7ee4549e-bbc2-46a5-9367-135491199be1
with_terminal() do
	println(parcels_in_each_office)
end

# ╔═╡ d4af5c29-c72d-41e5-8e70-d60364b4734b
@bind location Radio(["S1", "S2", "S3"])

# ╔═╡ d441bb7e-776e-4079-ae3d-3e9c464e73a5
with_terminal() do
	println(location)
end

# ╔═╡ c5b62cc6-d8ca-4ff0-aa44-f0296e55d172
function create_search(Transition_Model, ancestors, initial_state, goal_state)
	result = []
	visitor = goal_state
	while !(visitor == initial_state)
		current_state_ancestor = ancestors[visitor]
		related_transitions = Transition_Model[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == visitor
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		visitor = current_state_ancestor
	end
	return result
end

# ╔═╡ 41ba2100-b514-4e10-9494-4b1a8367cea8
md"### Implementation of the star algorithm"

# ╔═╡ f3843f6c-0eee-446c-8ce8-e542521c9ac2
function constructpath(S1, goal_state)
    return min(S1[1] - goal_state[1]) + min(S1[2] - goal_state[2])
end

# ╔═╡ 2ede32ac-db57-4011-987b-9d63b3fa5d13
md"### Setting Goal test"

# ╔═╡ e20e4447-ba2a-4cdb-af84-51209f9bafa0
function goal_test(currentState::State)
    return ! (currentState.parcels[1] || currentState.parcels[3])
end

# ╔═╡ f83c3906-b262-4646-a55b-3ab6e8d03379
with_terminal() do
	println( S10)
end

# ╔═╡ b66f8f33-7abe-431a-ae9f-5002e30557d0
function strategy(initial_state, transition_dict, is_goal,all_candidates,
add_candidate, remove_candidate)
	visited = []
	ancestors = Dict{State, State}()
	the_candidates = add_candidate(all_candidates, initial_state, 0)
	parent = initial_state
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			currentState = t1

			push!(visited, currentState)
			candidates = transition_dict[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in visited)
					push!(ancestors, single_candidate[2] => currentState)
					if (is_goal(single_candidate[2]))
						return create_search(transition_dict, ancestors,
initial_state, single_candidate[2])
					else
						the_candidates = add_candidate(the_candidates,
single_candidate[2], single_candidate[1].cost)
						end
					end
				end
			end
		end
end

# ╔═╡ 7841988a-d2e3-4c8d-a9bd-9543240536c4
function Astar(Transition_Model,initial_state, goal_state)
	
    result = []
	frontier = Queue{State}()
	explored = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initial_state)
	parent = initial_state
    while true
		if isempty(frontier)
			return []
		else
			currentState = dequeue!(frontier)
			push!(explored, currentState)
			candidates = Transition_Model[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return strategy(Transition_Model, parents,initial_state, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ 7e19536e-ef56-4e61-9039-d8d81541cfa3
md"### Implementation of Moves"

# ╔═╡ 8a148c7f-bf58-48b4-a573-478d3cded719
function add_to_queue(queue::Queue{State}, state::State, cost::Int64)
    enqueue!(queue, state)
    return queue
end

# ╔═╡ ce1c06e7-8ce3-4bde-8eaa-4ffcd2bf3512
function add_to_stack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ 3e193497-a704-4e4d-9569-63040d953edd
function remove_from_queue(queue::Queue{State})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ ed7ba151-2581-4434-b90c-fe30bbdb9f61
strategy(S1, Transition_Model, goal_test, Queue{State}(), add_to_queue, remove_from_queue)

# ╔═╡ a5ddd3a4-1c4d-4319-a015-950611a9a90a
function remove_from_stack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end

# ╔═╡ 0add72da-d2bb-4281-9227-c78606723647
function input(prompt::AbstractString)
    print(prompt)
    return readline()
end

# ╔═╡ Cell order:
# ╠═39a0af94-6c34-4e41-b0d1-0bfa179b1c93
# ╠═a08cb406-9453-4ce1-973f-228ad82937fb
# ╠═300c3d14-0858-4c1e-aa4e-a7dc9d15666d
# ╠═6fbce7f8-ff9e-4666-8652-d52b5d0d5a24
# ╠═c5f9cfc6-0d5f-4479-bb9a-4ab888263334
# ╠═16aa00d7-dee3-49df-9e19-4b8a1caa566f
# ╠═7c74761a-cd32-4ee9-b76c-bbe5cfb9cc58
# ╠═b8ed9cf3-07fe-4c90-a87b-0d012fc57910
# ╠═870a474b-532f-4a37-b808-0a9122ff5a3f
# ╠═c1542490-5b5a-4874-9a34-140d694f2c9c
# ╠═325d896b-9435-4a3c-9c3b-0cca0a6d31f1
# ╠═79c3c377-64db-4079-bcd1-4f3dcac7944d
# ╠═55fddaeb-3496-4f03-a70d-d83507afbd88
# ╠═5ae7f7a3-8848-44c8-ab8b-8e0dd22575e3
# ╠═d1ef21f4-d2c4-4e73-a318-7da539dc10da
# ╠═49240fc8-4694-4185-9f8f-a6168d46e38a
# ╠═c829a6dc-3e59-478a-b9d3-8c0b24c28706
# ╠═d0c6d99f-9080-4b56-a8f0-93d26ae28d5e
# ╠═24e97bc6-d419-4534-8a51-70bac191c281
# ╠═3bd8a2ca-ecf5-47e5-8187-07b862439725
# ╠═294ed3de-1688-437c-a67c-4031f5a85d02
# ╠═d8e5b6dd-ede8-4542-bae5-79bdda1b65a8
# ╠═1e474d08-a0bf-4f4f-a7d1-1650cda8cbf6
# ╠═b7b78c22-9b53-41da-8f20-0ff4e22d27ad
# ╠═fabc9a90-9717-4ab4-b774-77d93de01244
# ╠═7e9b68cd-79a6-4e14-b007-8e2f1640fc5e
# ╠═5d5358aa-6d43-4fbc-9556-b22a0f1b0030
# ╠═80c5988e-c141-49f2-a272-8689915d2473
# ╠═2bd6e9c1-dfd2-46fc-80ed-9b4228f0b888
# ╠═d75e3c1a-da82-45a5-b4c6-1fcd07f32af3
# ╠═2b1a9b5c-cafb-4a68-8ab7-eaf9dff462d8
# ╠═2f84da37-39c8-4cde-ae19-2c550b72caa5
# ╠═1456cfad-c1d5-4bec-b337-e6705d40b186
# ╠═da737b34-b067-46c9-93bc-cbde66244f9a
# ╠═7eec2e76-5d91-4def-926f-23eb3d031745
# ╠═c3e99177-bd08-4dae-aa08-d455781f86a7
# ╠═8ca45d09-2f5e-4da5-8eeb-723adb379f9d
# ╠═4958cae9-9d0f-4ddb-849c-dc6132465a43
# ╠═29a0dee6-a271-447e-97d6-cc617152f4d5
# ╠═3ae1c473-5967-4bb1-b0d5-31f1b92507e9
# ╠═5dffa43c-dc33-4cd7-b8cb-286e73e521ef
# ╠═a02c4a9b-798b-4cf4-8d93-fccadfb8caef
# ╠═fb3b021e-9aee-477d-896b-3f53fa755b7f
# ╠═3a8c7fc0-ae7d-4c85-9bba-3a8e9c4afe60
# ╠═7ee4549e-bbc2-46a5-9367-135491199be1
# ╠═d4af5c29-c72d-41e5-8e70-d60364b4734b
# ╠═d441bb7e-776e-4079-ae3d-3e9c464e73a5
# ╠═c5b62cc6-d8ca-4ff0-aa44-f0296e55d172
# ╠═41ba2100-b514-4e10-9494-4b1a8367cea8
# ╠═f3843f6c-0eee-446c-8ce8-e542521c9ac2
# ╠═7841988a-d2e3-4c8d-a9bd-9543240536c4
# ╠═2ede32ac-db57-4011-987b-9d63b3fa5d13
# ╠═e20e4447-ba2a-4cdb-af84-51209f9bafa0
# ╠═f83c3906-b262-4646-a55b-3ab6e8d03379
# ╠═b66f8f33-7abe-431a-ae9f-5002e30557d0
# ╠═ed7ba151-2581-4434-b90c-fe30bbdb9f61
# ╠═7e19536e-ef56-4e61-9039-d8d81541cfa3
# ╠═8a148c7f-bf58-48b4-a573-478d3cded719
# ╠═ce1c06e7-8ce3-4bde-8eaa-4ffcd2bf3512
# ╠═3e193497-a704-4e4d-9569-63040d953edd
# ╠═a5ddd3a4-1c4d-4319-a015-950611a9a90a
# ╠═0add72da-d2bb-4281-9227-c78606723647
