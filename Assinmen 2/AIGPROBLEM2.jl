### A Pluto.jl notebook ###
# v0.19.0

using Markdown
using InteractiveUtils

# ╔═╡ 75547c4b-b9a7-40aa-8f9b-5bc40f289f76
using Pkg

# ╔═╡ 3aa4cb26-0dda-4840-b3ad-1fe8059f2e22
Pkg.activate("Project.toml")

# ╔═╡ 0b44dd9f-a520-466e-a083-dbfddee8b19d
using PlutoUI

# ╔═╡ 39bf0879-5bc5-4c49-89fd-b4bfec8a6b7f
using DataStructures

# ╔═╡ a759e840-d384-464b-a7a4-0795e394d8fe
using Markdown

# ╔═╡ 9eadc3d0-615f-4ce1-a6ef-a51609b11d61
using InteractiveUtils

# ╔═╡ 8b8c8385-2c0e-449d-af45-09f3dad90a3e
using Printf

# ╔═╡ a250cb12-8772-4dcb-ad8a-8ab8c130b9bc
@enum Domain a b c d e

# ╔═╡ ad130354-09c7-4253-86b1-a44f5590078d
md"### Vetumbuavi Ndjavera
 * Student number 217121314
"

# ╔═╡ db38f562-c0f1-410f-862b-a68e44fa1678
md"### Creating Struct"

# ╔═╡ a6414e67-72b7-48bc-a82c-33a70a9f9c63
struct Constant_Function_Dict{V}
    value::V
end

# ╔═╡ b012cd0d-d37f-4995-bf68-c104c61d4030
mutable struct CSPDictionary
    dict::Union{Nothing, Dict, Constant_Function_Dict}
end

# ╔═╡ 8b354d2b-6999-4c7b-814a-7ac7b01700dc
abstract type AbstractCSP
end

# ╔═╡ c46ba358-6fea-41e9-9dea-920ffff090cb
mutable struct CSP <: AbstractCSP
	vars::AbstractVector
	domains::CSPDictionary
	neighbors::CSPDictionary
	constraints::Function
	initial::Tuple
	current_domains::Union{Nothing, Dict}
	nassigns::Int64
end

# ╔═╡ 445419e0-2853-43fa-90fc-bb93dd3d2d99
mutable struct CSPVar
	name::String
	value::Union{Nothing,Domain}
	forbidden_values::Vector{Domain}
	domain_count::Int64
end

# ╔═╡ c075cf22-612d-4668-a784-c6279f962478
struct graph{T <: Real,U}
    edges::Dict{Tuple{U,U},T}
    verts::Set{U}
end

# ╔═╡ d67f927f-bf05-4ecb-aee2-cac6ca657ce5
md"### Creating Methods"

# ╔═╡ 2677b8c7-5fff-438b-8eab-908e8a7b69b1
function Graph(edges::Vector{Tuple{U,U,T}}) where {T <: Real,U}
    vnames = Set{U}(v for edge in edges for v in edge[1:2])
    adjmat = Dict((edge[1], edge[2]) => edge[3] for edge in edges)
    return graph(adjmat, vnames)
end

# ╔═╡ 11b1494c-ffe0-4e5d-be23-56d2a6b689c3
begin
	vertices(g::graph) = g.verts
	edges(g::graph)    = g.edges
end

# ╔═╡ 579c5147-bacf-4ad1-b4d2-cb686e5e9c62
diff = rand(setdiff(Set([a,b,c,d]), Set([a,b])))

# ╔═╡ 39a532ec-5e08-47d4-8a91-c249fcc86be7
neighbours(g::graph, v) = Set((b, c) for ((a, b), c) in edges(g) if a == v)

# ╔═╡ 0af87165-1b78-445e-809d-83ec410bf09b
md"### Assign arguments"

# ╔═╡ a6ca5e5d-b263-4271-be7d-4168a0efd507
function assigning(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
    assignment[key] = val;
    problem.nassigns = problem.nassigns + 1;
    nothing;
end

# ╔═╡ b9d81d3e-9c85-4a3d-a2ee-39902c5a2684
md"### Unassign arguments"

# ╔═╡ fd6a3489-4965-446e-aa2c-672451079118
function unassigning(problem::T, key, assignment::Dict) where {T <: AbstractCSP}
    if (haskey(assignment, key))
        delete!(assignment, key);
    end
    nothing;
end

# ╔═╡ 22b90e21-9348-40bd-8501-a7d3247cae19
function error(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
    return count(
                (function(second_key)
                    return (haskey(assignment, second_key) &&
                        !(problem.constraints(key, val, second_key, assignment[second_key])));
                end),
                problem.neighbors[key]);
end

# ╔═╡ 85d47d75-be39-4506-83bf-fbda28802429
function display(problem::T, assignment::Dict) where {T <: AbstractCSP}
    println("Cons: ", problem, " with assignment: ", assignment);
    nothing;
end

# ╔═╡ f7536654-1b8f-4234-ba82-09b15b07a0f5
function actions(problem::T, state::Tuple) where {T <: AbstractCSP}
    if (length(state) == length(problem.vars))
        return [];
    else
        let
            local assignment = Dict(state);
            local var = problem.vars[findfirst((function(e)
                                        return !haskey(assignment, e);
                                    end), problem.vars)];
            return collect((var, val) for val in problem.domains[var]
                            if error(problem, var, val, assignment) == 0);
        end
    end
end

# ╔═╡ f30eb165-716f-44a2-be7d-74b16423362b
function presult(problem::T, state::Tuple, action::Tuple) where {T <: AbstractCSP}
    return (state..., act);
end

# ╔═╡ b5f57621-d66b-4bcb-9960-88bf6e4063a9
md"### Path Finder"

# ╔═╡ 9175ad3f-c19f-4ef2-aba0-d4ce3682f44e
function checkpath(g::graph{T,U}, source::U, dest::U) where {T, U}
    @assert source ∈ vertices(g) "$source is not a vertex in the graph"
 
    if source == dest return [source], 0 end
    # Initialize variables
    inf  = typemax(T)
    dist = Dict(v => inf for v in vertices(g))
    prev = Dict(v => v   for v in vertices(g))
    dist[source] = 0
    Q = copy(vertices(g))
    neigh = Dict(v => neighbours(g, v) for v in vertices(g))
 
    # Main loop
    while !isempty(Q)
        u = reduce((x, y) -> dist[x] < dist[y] ? x : y, Q)
        pop!(Q, u)
        if dist[u] == inf || u == dest break end
        for (v, cost) in neigh[u]
            alt = dist[u] + cost
            if alt < dist[v]
                dist[v] = alt
                prev[v] = u
            end
        end
    end
 
    # Return path
    rst, cost = U[], dist[dest]
    if prev[dest] == dest
        return rst, cost
    else
        while dest != source
            pushfirst!(rst, dest)
            dest = prev[dest]
        end
        pushfirst!(rst, dest)
        return rst, cost
    end
end

# ╔═╡ 3c7b3889-f913-4517-a19b-23de2412da86
md"### Goal test set"

# ╔═╡ 7d65b127-f7c7-43d8-b656-899f1e5842ad
function goalTest(problem::T, state::Dict) where {T <: AbstractCSP}
    let
        local assignment = deepcopy(state);
        return (length(assignment) == length(problem.vars) &&
                all((function(key)
                            return error(problem, key, assignment[key], assignment) == 0;
                        end),
                        problem.vars));
    end
end

# ╔═╡ ac318de8-76c4-417e-961e-5f68090b99ed
md"### cost set"

# ╔═╡ 4035cdb9-91a1-4b9e-902c-059c5277201e
function path_cost(problem::T, cost::Float64, state1::Tuple, action::Tuple, state2::Tuple) where {T <: AbstractCSP}
    return cost + 1;
end

# ╔═╡ 064c7619-2148-4013-b4a2-1d18e4cf00da
md"### Pruning method"

# ╔═╡ 651b40d4-3390-40cc-b696-3ae1ee58fd5c
function pruning(problem::T, key, value, removals) where {T <: AbstractCSP}
    local not_removed::Bool = true;
    for (i, element) in enumerate(problem.current_domains[key])
        if (element == value)
            deleteat!(problem.current_domains[key], i);
            not_removed = false;
            break;
        end
    end
    if (not_removed)
        error("Could not find ", value, " in ", problem.current_domains[key], " for key '", key, "' to be removed!");
    end
    if (!(typeof(removals) <: Nothing))
        push!(removals, Pair(key, value));
    end
    nothing;
end

# ╔═╡ 1d727de2-4d14-41a2-aeef-911ede6b7a10
function errorvariables(problem::T, current_assignment::Dict) where {T <: AbstractCSP}
    return collect(var for var in problem.vars
                    if (error(problem, var, current_assignment[var], current_assignment) > 0));
end

# ╔═╡ b0f12650-22e8-4561-aecb-2255727d9ad7
md"### Forward check method"

# ╔═╡ 8dc2748a-51f4-402a-9296-19f9cc2d031c
function forwardchecking(problem::T, var, value, assignment::Dict, removals::Union{Nothing, AbstractVector}) where {T <: AbstractCSP}
    for B in problem.neighbors[var]
        if (!haskey(assignment, B))
            for b in copy(problem.current_domains[B])
                if (!problem.constraints(var, value, B, b))
                    pruning(problem, B, b, removals);
                end
            end
            if (length(problem.current_domains[B]) == 0)
                return false;
            end
        end
    end
    return true;
end

# ╔═╡ f812b57e-2482-479b-befe-dd56b1ddd576
md"### backtrack Method"

# ╔═╡ 4e788d3a-68ba-4598-a519-7c95875ab3ca
function backtrack(problem::T, assignment::Dict;
                    select_unassigned_variable::Function=first_unassigned_variable,
                    order_domain_values::Function=unordered_domain_values,
                    inference::Function=no_inference) where {T <: AbstractCSP}
    if (length(assignment) == length(problem.vars))
        return assignment;
    end
    local var = select_unassigned_variable(problem, assignment);
    for value in order_domain_values(problem, var, assignment)
        if (nconflicts(problem, var, value, assignment) == 0)
            assign(problem, var, value, assignment);
            removals = suppose(problem, var, value);
            if (inference(problem, var, value, assignment, removals))
                result = backtrack(problem, assignment,
                                    select_unassigned_variable=select_unassigned_variable,
                                    order_domain_values=order_domain_values,
                                    inference=inference);
                if (!(typeof(result) <: Nothing))
                    return result;
                end
            end
            reserve(problem, removals);
        end
    end
    unassign(problem, var, assignment);
    return nothing;
end

# ╔═╡ ccf22db3-5efd-446e-b8b6-1bf5060f510e
function assignment(problem::T) where {T <: AbstractCSP}
    support(problem);
    return Dict(collect(Pair(key, problem.current_domains[key][1])
                        for key in problem.vars
                            if (1 == length(problem.current_domains[key]))));
end

# ╔═╡ 7e29a896-8725-43c5-afb0-64dc548afca8
testGraph = [("a", "b", 1), ("b", "e", 2), ("a", "e", 4)]

# ╔═╡ 8ed3e19c-1d49-44d5-b659-147c4665452f
g = Graph(testGraph)

# ╔═╡ 5b6f0621-62d2-4c9a-af7f-d956d2f61e44
src, dst = "a", "e"

# ╔═╡ 1ca557f2-78fc-495c-9178-70b7fa03a01e
path, cost = checkpath(g, src, dst)

# ╔═╡ 00ff6e8d-71ce-42fc-a994-7affe4188ae2
with_terminal() do
	@printf("\n%3s | %2s | %s\n", "src", "dst", "path")
    @printf("-----------------\n")
	for src in vertices(g), dst in vertices(g)
    path, cost = checkpath(g, src, dst)
    @printf("%3s | %2s | %s\n", src, dst, isempty(path) ? "no possible path" : join(path, " → ") * " ($cost)")
	end
end

# ╔═╡ a8963319-a0e4-4077-8607-8badc02b0fe0
with_terminal() do
	println("Shortest path from $src to $dst: ", isempty(path) ? "no possible path" : join(path, " → "), " (cost $cost)")
end

# ╔═╡ Cell order:
# ╠═75547c4b-b9a7-40aa-8f9b-5bc40f289f76
# ╠═3aa4cb26-0dda-4840-b3ad-1fe8059f2e22
# ╠═0b44dd9f-a520-466e-a083-dbfddee8b19d
# ╠═39bf0879-5bc5-4c49-89fd-b4bfec8a6b7f
# ╠═a759e840-d384-464b-a7a4-0795e394d8fe
# ╠═9eadc3d0-615f-4ce1-a6ef-a51609b11d61
# ╠═8b8c8385-2c0e-449d-af45-09f3dad90a3e
# ╠═a250cb12-8772-4dcb-ad8a-8ab8c130b9bc
# ╠═ad130354-09c7-4253-86b1-a44f5590078d
# ╠═db38f562-c0f1-410f-862b-a68e44fa1678
# ╠═a6414e67-72b7-48bc-a82c-33a70a9f9c63
# ╠═b012cd0d-d37f-4995-bf68-c104c61d4030
# ╠═8b354d2b-6999-4c7b-814a-7ac7b01700dc
# ╠═c46ba358-6fea-41e9-9dea-920ffff090cb
# ╠═445419e0-2853-43fa-90fc-bb93dd3d2d99
# ╠═c075cf22-612d-4668-a784-c6279f962478
# ╠═d67f927f-bf05-4ecb-aee2-cac6ca657ce5
# ╠═2677b8c7-5fff-438b-8eab-908e8a7b69b1
# ╠═11b1494c-ffe0-4e5d-be23-56d2a6b689c3
# ╠═579c5147-bacf-4ad1-b4d2-cb686e5e9c62
# ╠═39a532ec-5e08-47d4-8a91-c249fcc86be7
# ╠═0af87165-1b78-445e-809d-83ec410bf09b
# ╠═a6ca5e5d-b263-4271-be7d-4168a0efd507
# ╠═b9d81d3e-9c85-4a3d-a2ee-39902c5a2684
# ╠═fd6a3489-4965-446e-aa2c-672451079118
# ╠═22b90e21-9348-40bd-8501-a7d3247cae19
# ╠═85d47d75-be39-4506-83bf-fbda28802429
# ╠═f7536654-1b8f-4234-ba82-09b15b07a0f5
# ╠═f30eb165-716f-44a2-be7d-74b16423362b
# ╠═b5f57621-d66b-4bcb-9960-88bf6e4063a9
# ╠═9175ad3f-c19f-4ef2-aba0-d4ce3682f44e
# ╠═3c7b3889-f913-4517-a19b-23de2412da86
# ╠═7d65b127-f7c7-43d8-b656-899f1e5842ad
# ╠═ac318de8-76c4-417e-961e-5f68090b99ed
# ╠═4035cdb9-91a1-4b9e-902c-059c5277201e
# ╠═064c7619-2148-4013-b4a2-1d18e4cf00da
# ╠═651b40d4-3390-40cc-b696-3ae1ee58fd5c
# ╠═1d727de2-4d14-41a2-aeef-911ede6b7a10
# ╠═b0f12650-22e8-4561-aecb-2255727d9ad7
# ╠═8dc2748a-51f4-402a-9296-19f9cc2d031c
# ╠═f812b57e-2482-479b-befe-dd56b1ddd576
# ╠═4e788d3a-68ba-4598-a519-7c95875ab3ca
# ╠═ccf22db3-5efd-446e-b8b6-1bf5060f510e
# ╠═7e29a896-8725-43c5-afb0-64dc548afca8
# ╠═8ed3e19c-1d49-44d5-b659-147c4665452f
# ╠═5b6f0621-62d2-4c9a-af7f-d956d2f61e44
# ╠═1ca557f2-78fc-495c-9178-70b7fa03a01e
# ╠═00ff6e8d-71ce-42fc-a994-7affe4188ae2
# ╠═a8963319-a0e4-4077-8607-8badc02b0fe0
